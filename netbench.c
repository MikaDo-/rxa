/**
 * Alexis BEAUJET - M1S2 RXA
 *
 * Serveur de chat + benchmark réseau
 *
 * Client à utiliser : ./client
 *
 * Lancement : ./netbench port_number
 * Lorsqu'un message est envoyé au serveur, il est envoyé à tous les clients connectés.
 * Le nombre maximal de clients est inscrit en dur dans le code.
 * 
 * Dans le cas d'une commande émise par un client : seul le client concerné
 * obtient la progression et le résultat.
 * 
 * Commandes :
 *      /bye         : déconnexion
 *      /echo xxx    : retourne xxx octets envoyés par le client au fur et à mesure qu'ils sont reçus.
 *      /ack  xxx    : reçoit xxx octets de la part du client, et confirme la réception une fois terminée.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MAX_CLIENTS 10
#define BUFFER_SIZE 256
#define ERR_WRITE error("Cannor write on client socket.\n", 6);
#define ERR_READ error("Cannor read on client socket.\n", 7);

int serv_sock;
// Tableau de toutes les sockets des clients.
// Pour savoir si un client peut se connecter ou non, on regarde si un emplacement est libre dans ce tableau.
// L'ID du client est l'indice du premier emplacement libre dans ce tableau.
// Détail dans make_client_id();
int*   active_clients;
// tableau des pseudonymes
char** client_nicks;

// Messages d'interface :
char* user_says_format	= "%s dit : %s\n";
char* server_full 		= "Connection refused : server is full.\n";
char* welcome_message	= "Bonjour client %02d. \n";
char* prompt_message 	= "Saisissez votre message : ";
char* log_format 		= "Message du client %02d : %s";
char* message_ack 		= "Message reçu.\n";
char* command_ack 		= "Commande executee.\n";
char* command_nack 		= "Erreur lors de l'execution de la commande.\n";
char* login_format 		= "%s s'est connecte.\n";
char* logout_format 	= "%s s'est deconnecte.\n";

void error(const char *message, int err_code){
    fprintf(stderr, message);
    exit(err_code);
}

/*
 * Trouve un emplacement de libre dans le tableau
 * de clients actifs (active_clients). Le premier enmplacement vide est
 * retourné. Sinon, on retourne -1.
 */
int make_client_id(){
    for(int i = 0;i<MAX_CLIENTS;i++)
        if(active_clients[i] == 0)
            return i;
    return -1;
}

/*
 * Diffusion d'un message à tous les AUTRES ustilisateurs actifs.
 */
void broadcast(int client_id, const char* message, bool says){
    int client;
    char buffer[BUFFER_SIZE];
	if(says)
		sprintf(buffer, user_says_format, client_nicks[client_id], message);
	else
		strcpy(buffer, message);
    int mess_len = strlen(buffer);
    for(int i = 0;i<MAX_CLIENTS;i++)
        if( (client = active_clients[i]) != 0 && i != client_id)
            if( write(client, buffer, mess_len) == 0)
                ERR_WRITE
}

// Si le client se déconnecte.:w
int client_disconnects(int client_id, char* client_buffer){
    sprintf(client_buffer, logout_format, client_nicks[client_id]);
    broadcast(client_id, (const char*) client_buffer, false);
    write(active_clients[client_id], "bye!\n", sizeof("bye!\n"));
    close(active_clients[client_id]);
    active_clients[client_id] = 0;
    return 1;
}

int net_bench(int client_id, size_t size, int echo){
    int client_socket = active_clients[client_id];
    write(client_socket, "begin", sizeof("begin"));
    if(echo){
        char c;
        for(size_t i = 0;i<size;i++){
            if(read(client_socket, &c, 1) != 1){
                printf("Erreur pendant read echo\n");
                return -1;
            }
            if(write(client_socket, &c, 1) == -1){
                printf("Erreur pendant write echo\n");
                return -1;
            }
        }
    }else{
        size_t received = 0;
        char* buff = (char*)malloc(size);
        do{
            int res;
            if( (res = read(client_socket, buff, size-received)) == -1){
                printf("Erreur pendant read ack\n");
            }else
                received += res;
        }while(received != size);
        free(buff);
    }
    write(client_socket, "ack", 4);
    return 1;
}

int full_duplex_bench(int client_id, char* client_buffer){
    size_t size;
    sscanf(client_buffer, "/echo %zd", &size);
    return net_bench(client_id, size, 1);
}

int half_duplex_bench(int client_id, char* client_buffer){
    size_t size;
    sscanf(client_buffer, "/ack %zd", &size);
    return net_bench(client_id, size, 0);
}

void set_client_nickname(int client_id, char* nick){
	strncpy(client_nicks[client_id], nick, 20);
	client_nicks[20] = '\0';
}

int client_nickname(int client_id, char* client_buffer){
	char nickname[20+1];
	char old_nick[20+1]; // hehe, old nick...
	strncpy(old_nick, client_nicks[client_id], 20);
    if(sscanf(client_buffer, "/nick %20s", nickname) < 1)
		return -1;
	set_client_nickname(client_id, nickname);
	sprintf(client_buffer, "Nickname changed to %s.\n", nickname);
	write(active_clients[client_id], client_buffer, strlen(client_buffer));

	sprintf(client_buffer, "%s changed his/her nickname to %s.\n", old_nick, nickname);
	broadcast(client_id, client_buffer, false);
	return 1;
}

/**
 * Les fonctions de traitement des commandes retournent :
 *      1 en cas de bonne exécution
 *     -1 en cas de foirage
 */
typedef struct {
    const char* command;
    const char* arg_format;
    const int arg_count;
    int(*func)(int client_id, char* client_buffer);
} command_t;

const command_t available_commands[] = {
    {"/bye",     NULL,  0,  client_disconnects },
    {"/echo",   "%zd",  1,  full_duplex_bench },
    {"/ack",    "%zd",  1,  half_duplex_bench },
    {"/nick",   "%s",   1,  client_nickname }
};

int command_parser(int client_id, char* client_buffer){
    char command_format[30];
    size_t bench_size;
    char sscanf_buffer[10];
    static int command_count = sizeof(available_commands)/sizeof(command_t);

    if(client_buffer[0] == '/') // s'il s'agit d'une commande
		for(int i = 0;i<command_count;i++){
			const command_t* com = &(available_commands[i]);
			if(strncmp(client_buffer, com->command, strlen(com->command)) == 0){
				if(com->arg_count == 0 && com->arg_format == NULL){
					return com->func(client_id, client_buffer);
				}else{
					sprintf(command_format, "%s %s", com->command, com->arg_format);
					if(sscanf(client_buffer, command_format, sscanf_buffer, sscanf_buffer, sscanf_buffer, sscanf_buffer) >= com->arg_count){
						return com->func(client_id, client_buffer);
					}
				}
			}
		}
    // si aucune commande n'est executée, c'est un message et on le diffuse.
    broadcast(client_id, (const char*) client_buffer, true);
    printf(log_format, client_id, client_buffer);
    return 0;
}

/*
 * Boucle de dialogue pour un client.
 */
void *handle_client(void* client_id_pt){
    int client_id = *(int*)client_id_pt;
    int client_sock = active_clients[client_id];
    char client_buffer[BUFFER_SIZE] = {0};

	// on envoie un message d'accueil au nouveau client.
    /* supprimé pour le client benchmark
    sprintf(client_buffer, welcome_message, client_id);
    if( write(client_sock, client_buffer, strlen(client_buffer)+1) <0 )
        ERR_WRITE
    */
	// On signale aux autres clients qu'un nouvel utilisateur est connecté.
	sprintf(client_buffer, login_format, client_nicks[client_id]);
	broadcast(client_id, (const char*) client_buffer, false);

    while(active_clients[client_id] != 0){
		// on prompt l'utilisateur pour son message
		/* supprimé pour le client benchmark
        sprintf(client_buffer, prompt_message);
        if( write(client_sock, client_buffer, strlen(client_buffer)+1) <0 )
            ERR_WRITE*/

		// on lit son message
        memset(client_buffer, 0, BUFFER_SIZE);
        if( read(client_sock, client_buffer, BUFFER_SIZE) < 0 )
            ERR_READ

        int res = command_parser(client_id, client_buffer);
        switch(res){
            case 0:
                //if( write(client_sock, message_ack, strlen(message_ack)+1) <0 )
                //    ERR_WRITE
                break;
            case 1:
                //if( write(client_sock, command_ack, strlen(command_ack)) <0 )
                //    ERR_WRITE
                break;
            case -1:
				printf("MERDE LLLLLLLLEEEEEEEELLLLLLL\n");
                //if( write(client_sock, command_nack, strlen(command_nack)+1) <0 )
                //    ERR_WRITE
                break;
            default:
                error("Error during message processing LEL :(", 8);
                break;
        };
    }
	return NULL;
}

int init_server(int port){
	int serv_sock = 0;
	struct sockaddr_in serv_addr;
		
    if( (serv_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0)
        error("Error, could not create a socket.\n", 3);

    serv_addr = (struct sockaddr_in) {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(port)
    };

	int reuse_port = 1;
	setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_port, sizeof(reuse_port));

    if(bind(serv_sock, (const struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		return -1;

	return serv_sock;
}

int client_id_from_socket(int socket){
    for(int i = 0;i<MAX_CLIENTS;i++)
        if(active_clients[i] == socket)
            return i;
	return -1;
}

// n fait c'est foiré... je laisse au cas où, en attendant ça ne fait pas de mal.
void *stop_thread_func(void* arg){
	char cmd[5];
	scanf("%5s", cmd);
	fflush(stdin);
	printf("%s\n", cmd);
	if(strcmp("stop", cmd) == 0)
		close(serv_sock);
	return NULL;
}

void sigpipe_handler(int c){
    puts("SIGPIPE CAUGHT !\n");
    char* argv[2] = {"./netbench", "3000"};
    main(2, argv);
}

int main(int argc, char* argv[]){
    signal(SIGPIPE, sigpipe_handler);

	int serv_port = 0, new_client_socket;
    char buffer[BUFFER_SIZE] = {0};
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen;

	// Initialisation des attributs du serveur.
    active_clients = (int *)malloc(MAX_CLIENTS*sizeof(int));
    client_nicks = (char **)malloc(MAX_CLIENTS*sizeof(char*));
    memset(active_clients, 0, MAX_CLIENTS*sizeof(int));
    memset(client_nicks, 0, MAX_CLIENTS*sizeof(char*));
	for(int i = 0;i<MAX_CLIENTS;i++)
		client_nicks[i] = (char*)malloc(20);
    pthread_t client_threads[MAX_CLIENTS];

    if(argc != 2)
        error("Error, must provide port number as argument.\n", 2);
    serv_port = atoi(argv[1]);

	if( (serv_sock = init_server(serv_port)) > 0)
		printf("Server listening on port %d.\n", serv_port);
	else
        error("ERROR on server init\n", 4);

	// On lance l'écoute avec une file d'attente de 5 clients max.
    listen(serv_sock, 5);
    clilen = sizeof(cli_addr);
	/*pthread_t stop_thread;
	pthread_create(&stop_thread, NULL, stop_thread_func, NULL);*/
    while(new_client_socket = accept(serv_sock, (struct sockaddr*) &cli_addr, &clilen)){

        if(new_client_socket < 0)
            error("Cannot accept connection.", 5);
        
		// On attribue un id au nouveau client. Utile pour retrouver sa socket
		// par la suite, et conserver un nombre limité de clients.
        int client_id = make_client_id();
        if(client_id == -1 ){
            printf(server_full);
            write(new_client_socket , server_full, strlen(server_full));
            close(new_client_socket);
            continue;
        }

        active_clients[client_id] = new_client_socket;

		//sprintf(buffer, "client_%02d", client_id);
		//set_client_nickname(client_id, buffer);

		// On prend en charge le client dans un nouveau thread que l'on détache (libération du stack du thread)
        pthread_create(&client_threads[client_id], NULL, handle_client, &client_id);
		pthread_detach(client_threads[client_id]);

    }
    perror("Accept : ");
    printf("errno : %d", errno);
    close(serv_sock);
    
    return 0;
}
