.PHONY: all
all: exo2 exo3 netbench client

exo2: exo2.c
	gcc -std=c99 -g -o exo2 exo2.c -lpthread -lm

exo3: exo3.c
	gcc -std=c99 -g -o exo3 exo3.c -lpthread -lm

netbench: netbench.c
	gcc -std=c99 -g -o netbench netbench.c -lpthread -lm

client: client.c
	gcc -std=c99 -g -o client client.c -lpthread -lm

clean:
	rm exo2 exo3 netbench client
