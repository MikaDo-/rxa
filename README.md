
Alexis BEAUJET - M1S2 RXA
============================================

Utilisation :
-------------

Clients : netcat, telnet...

Lancement : ./exoX port_number
Lorsqu'un message est envoyé au serveur, il est envoyé à tous les clients connectés.
Le nombre maximal de clients est encore inscrit en dur dans le code.

Commande de déconnexion : STOP

Exo 2 - Serveur de chat multithreadé basique.
---------------------------------------------

Réponses aux questions :
1. Un nouveau thread doit être créé à chaque nouvelle connexion entrante. Ainsi, chaque connexion dispose de son propre dialogue avec le serveur. Cette méthode a le mérite de favoriser la rapidité du traitement (même si la création d'un thread a un coût), mais gaspille des resources dans le sens où chaque thread est endormi entre chaque lecture de la socket du client.
2. La réception des chaînes de caractères se fait via la primitive read. Selon UNIX, tout est un fichier. Il suffit alors de disposer du descripteur de fichier de la socket du client pour lire dessus avec read.
3. Le serveur conserve en permanence une liste des clients connectés. Ainsi, pour chaque message reçu, le serveur s'occupe de le diffuser par des write sur les sockets des autres clients.
4. La fonction de dialogue avec le client doit détecter le mot-clé (ici STOP, oups :/ ). Une fois la commande détecté, on diffuse l'information aux autres clients, on ferme la socket du client concerné, puis on libère le slot pour permettre d'autres connexion.

Détail de l'implémentation :
Lors de la connexion d'un client, on crée un thread qui lui sera dédié par la suite.
Ce thread sera à faire le dialogue : demander le mesage, le lire, donner un accusé de réception et le dispatcher aux autres clients.
Lorsqu'on est en attente d'un message de la part du client, son thread dort sur un appel bloquant (ici read).

On a donc un 1 + n_clients threads exécutés sur ce modèle de serveur.


Exo 3 - Serveur de chat évenementiel.
-------------------------------------

Réponses aux questions :
1. Dans ce modèle de programmation, toutes les sockets (y compris celle du serveur) sont surveillées en lecture. Aussi, dès qu'une nouvelle connexion est établie avec le serveur, la socket de ce dernier devient prête en lecture. Il suffit donc de faire un accept sur la socket du serveur dès qu'elle est prête en lecture.
2. Les sockets des clients sont aussi surveillés. Lorsqu'on détecte qu'un des descripteurs de fichiers surveillés est prêt en lecture, on les parcourt tous. Si une socket de client est prête, on lance une lecture dessus, puis on diffuse aux autres de la même manière que dans l'exo précédent.
3. Avec cette façon de procéder l'utilisation du réseau en termes de charge est identique.
Quelle démarche adopter vis-à-vis de quoi pour l'utilisation d'IP ? :(
Il est toujours possible d'utiliser telnet et netcat.

Ce serveur ne fonctionne que sur un seul fil d'exécution. Le multiplexage des connexions sur un seul thread repose sur la primitve 'select'.
Le serveur dispose d'un set de sockets à surveiller en lecture, qui correspond à celle du serveur et une par client connecté. C'est à dire, on souhaite lire sur une liste de sockets dès qu'il y en a une de prête.
Dès qu'une socket est lisible, on effectue le traitement adéquat (ici read, broadcast et prompt)


