/**
 * Alexis BEAUJET - M1S2 RXA
 *
 * Exo 3 - Serveur de chat évenementiel.
 *
 * Client à utiliser : telnet
 *
 * Lancement : ./chat port_number
 * Lorsqu'un message est envoyé au serveur, il est envoyé à tous les clients connectés.
 * Le nombre maximal de clients est inscrit en dur dans le code.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>

#define MAX_CLIENTS 10
#define ERR_WRITE error("Cannor write on client socket.\n", 6);
#define ERR_READ error("Cannor read on client socket.\n", 6);

// tableau de toutes les sockets des clients.
// pour savoir si un client peut se connecter ou non, on regarde si un emplacement est libre dans ce tableau.
// L'ID du client est l'indice du premier emplacement libre dans ce tableau.
int* active_clients;

void error(const char *message, int err_code){
    fprintf(stderr, message);
    exit(err_code);
}

int make_client_id(){
    for(int i = 0;i<MAX_CLIENTS;i++)
        if(active_clients[i] == 0)
            return i;
    return -1;
}

void broadcast(int client_id, const char* message){
    int client;
    char buffer[256];
    sprintf(buffer, "Client %02d dit : ", client_id);
    strcat(buffer, message);
    strcat(buffer, "\n");
    int mess_len = strlen(buffer);
    for(int i = 0;i<MAX_CLIENTS;i++)
        if( (client = active_clients[i]) != 0)
            if( write(client, buffer, mess_len) == 0)
                ERR_WRITE
}

void* client_welcome(void* client_id){
    char buffer[256];
    char welcome_message[] = "Bonjour client %02d. ";
    sprintf(buffer, welcome_message, *(int*)client_id);
    write(active_clients[*(int*)client_id], buffer, strlen(buffer));
}

int client_read(int client_id){
    int client_sock = active_clients[client_id];
    char client_buffer[256] = {0};

    char prompt_message[] 	 = "Saisissez votre message : ";
    char log_format[] 		 = "Message du client %02d : %s";
    char logout_format[] 	 = "Deconnexion du client %02d.";
    char cli_logout_format[] = "Deconnexion.";
    char message_ack[] 		 = "Message reçu.\n";

	// if the client disconnects
	if( read(client_sock, client_buffer, 256) < 0 || strncmp(client_buffer, "STOP", 4) == 0){
		active_clients[client_id] = 0;
		sprintf(client_buffer, logout_format, client_id);
		puts(client_buffer);
		broadcast(client_id, (const char*) client_buffer);
		sprintf(client_buffer, cli_logout_format, client_id);
		if( write(client_sock,  client_buffer, strlen(client_buffer)) == 0)
			ERR_WRITE
		close(client_sock);
		return 2;
	}

	broadcast(client_id, (const char*) client_buffer);

	printf(log_format, client_id, client_buffer);

	// send acknowledgement message.
	if( write(client_sock, message_ack, strlen(message_ack)) <0 )
		ERR_WRITE

	// prompt for a new message.
	sprintf(client_buffer, prompt_message);
	if( write(client_sock, client_buffer, strlen(client_buffer)) <0 )
		ERR_WRITE

	memset(client_buffer, 0, 256);

	return 0;
}

int init_server(int port){
	int serv_sock = 0;
	struct sockaddr_in serv_addr;
		
    if( (serv_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0)
        error("Error, could not create a socket.\n", 3);

    serv_addr = (struct sockaddr_in) {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(port)
    };

	int reuse_port = 1;
	setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_port, sizeof(reuse_port));

    if(bind(serv_sock, (const struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		return -1;

	return serv_sock;
}

int client_id_from_socket(int socket){
    for(int i = 0;i<MAX_CLIENTS;i++)
        if(active_clients[i] == socket)
            return i;
	return -1;
}

int main(int argc, char* argv[]){

    active_clients = (int *)malloc(MAX_CLIENTS*sizeof(int));
    memset(active_clients, 0, MAX_CLIENTS*sizeof(int));
    pthread_t client_threads[MAX_CLIENTS];
    char* server_full = "Connection refused : server is full.\n";

    int serv_sock = 0, serv_port = 0, newsockfd;
    char buffer[256] = {0};
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen = sizeof(cli_addr);

    if(argc != 2)
        error("Error, must provide port number as argument.\n", 2);

    serv_port = atoi(argv[1]);
	
	if( (serv_sock = init_server(serv_port)) > 0)
		printf("Server listening on port %d.\n", serv_port);
	else
        error("ERROR on server init\n", 4);

    listen(serv_sock, 5);

	fd_set active_connections, read_connections;
	FD_ZERO(&active_connections);
	FD_SET(serv_sock, &active_connections);


	while(1){
		// on attend qu'une des sockets soit prête en lecture.
		read_connections = active_connections;
		if( select(FD_SETSIZE, &read_connections, NULL, NULL, NULL) > 0 ){
			for(int i=0;i<FD_SETSIZE;i++){
				if(FD_ISSET(i, &read_connections)){

					// en cas de nouvelle connexion :
					if(i == serv_sock){
						printf("New connection received.\n");
						newsockfd = accept(serv_sock, (struct sockaddr*) &cli_addr, &clilen);

						if(newsockfd < 0)
							error("Cannot accept connection.", 5);
						
						int client_id = make_client_id();
						if(client_id == -1 ){
							printf(server_full);
							write(newsockfd, server_full, strlen(server_full));
							close(newsockfd);
							continue;
						}

						FD_SET(newsockfd, &active_connections);

						active_clients[client_id] = newsockfd;

						client_welcome(&client_id);
						pthread_create(&client_threads[client_id], NULL, client_welcome, &client_id);
					}else{ // donnée arrivant sur une connexion existante :
						int client_id = client_id_from_socket(i);
						printf("READ ON CLIENT %d, SOCKET %d\n", client_id, i);
						//pthread_create(&client_threads[client_id], NULL, (stupid cast)client_read, client_id);
						if(client_read(client_id) == 2)
							FD_CLR(i, &active_connections);
					}
				}
			}
		}else{
			// timeout during select.
		}
    }
    close(serv_sock);
    
    return 0;
}
