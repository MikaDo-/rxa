/**
 * Alexis BEAUJET - M1S2 RXA
 *
 * Serveur de chat multithreadé basique.
 *
 * Client à utiliser : telnet
 *
 * Lancement : ./chat port_number
 * Lorsqu'un message est envoyé au serveur, il est envoyé à tous les clients connectés.
 * Le nombre maximal de clients est inscrit en dur dans le code.
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define MAX_CLIENTS 10
#define BUFFER_SIZE 256
#define ERR_WRITE error("Cannor write on client socket.\n", 6);
#define ERR_READ error("Cannor read on client socket.\n", 7);

// Tableau de toutes les sockets des clients.
// Pour savoir si un client peut se connecter ou non, on regarde si un emplacement est libre dans ce tableau.
// L'ID du client est l'indice du premier emplacement libre dans ce tableau.
int* active_clients;

// Messages d'interface :
char* user_says_format	= "Client %02d dit : %s\n";
char* server_full 		= "Connection refused : server is full.\n";
char* welcome_message	= "Bonjour client %02d. ";
char* prompt_message 	= "Saisissez votre message : ";
char* log_format 		= "Message du client %02d : %s";
char* message_ack 		= "Message reçu.\n";
char* logout_command 	= "STOP";
char* login_format 		= "Client %02d s'est connecte.";
char* logout_format 	= "Client %02d s'est deconnecte.";


void error(const char *message, int err_code){
    fprintf(stderr, message);
    exit(err_code);
}

/*
 * Trouve un emplacement de libre dans le tableau
 * de clients actifs (active_clients). Le premier enmplacement vide est
 * retourné. Sinon, on retourne -1.
 */
int make_client_id(){
    for(int i = 0;i<MAX_CLIENTS;i++)
        if(active_clients[i] == 0)
            return i;
    return -1;
}

/*
 * Diffusion d'un message à tous les AUTRES ustilisateurs actifs.
 */
void broadcast(int client_id, const char* message){
    int client;
    char buffer[BUFFER_SIZE];
    sprintf(buffer, user_says_format, client_id, message);
    int mess_len = strlen(buffer);
    for(int i = 0;i<MAX_CLIENTS;i++)
        if( (client = active_clients[i]) != 0 && i != client_id)
            if( write(client, buffer, mess_len) == 0)
                ERR_WRITE
}

/*
 * Boucle de dialogue pour un client.
 */
void handle_client(int* client_id_pt){
    int client_id = *client_id_pt;
    int client_sock = active_clients[client_id];
    char client_buffer[BUFFER_SIZE] = {0};

	// on envoie un message d'accueil au nouveau client.
    sprintf(client_buffer, welcome_message, client_id);
    if( write(client_sock, client_buffer, strlen(client_buffer)) <0 )
        ERR_WRITE

	// On signale aux autres clients qu'un nouvel utilisateur est connecté.
	sprintf(client_buffer, login_format, client_id);
	broadcast(client_id, (const char*) client_buffer);

    int client_alive = 1;

    while(client_alive == 1){
		// on prompt l'utilisateur pour son message
        sprintf(client_buffer, prompt_message);
        if( write(client_sock, client_buffer, strlen(client_buffer)) <0 )
            ERR_WRITE

		// on lit son message
        memset(client_buffer, 0, BUFFER_SIZE);
        if( read(client_sock, client_buffer, BUFFER_SIZE) < 0 )
            ERR_READ

		// si un STOP est reçu
        if(strncmp(client_buffer, logout_command, 4) == 0){
			sprintf(client_buffer, logout_format, client_id);
            client_alive = 0;
			broadcast(client_id, (const char*) client_buffer);
        } // sinon on broadcast le message
		else{
			broadcast(client_id, (const char*) client_buffer);
			printf(log_format, client_id, client_buffer);
		}

		// on retourne un accusé de réception au client.
        if( write(client_sock, message_ack, strlen(message_ack)) <0 )
            ERR_WRITE
    }
    close(client_sock);
    active_clients[client_id] = 0;
}

int main(int argc, char* argv[]){

    int serv_sock = 0, serv_port = 0, new_client_socket;
    char buffer[BUFFER_SIZE] = {0};
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen;

	// Initialisation des attributs du serveur.
    active_clients = (int *)malloc(MAX_CLIENTS*sizeof(int));
    memset(active_clients, 0, MAX_CLIENTS*sizeof(int));
    pthread_t client_threads[MAX_CLIENTS];

    if(argc != 2)
        error("Error, must provide port number as argument.\n", 2);
    serv_port = atoi(argv[1]);

    if( (serv_sock = socket(AF_INET, SOCK_STREAM, 0)) == 0)
        error("Error, could not create a socket.\n", 3);

    serv_addr = (struct sockaddr_in) {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(serv_port)
    };

    if( bind(serv_sock, (const struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding\n", 4);

    printf("Server listening on port %d.\n", serv_port);

	// On lance l'écoute avec une file d'attente de 5 clients max.
    listen(serv_sock, 5);
    clilen = sizeof(cli_addr);
    while(new_client_socket = accept(serv_sock, (struct sockaddr*) &cli_addr, &clilen)){

        if(new_client_socket < 0)
            error("Cannot accept connection.", 5);
        
		// On attribue un id au nouveau client. Utile pour retrouver sa socket
		// par la suite, et conserver un nombre limité de clients.
        int client_id = make_client_id();
        if(client_id == -1 ){
            printf(server_full);
            write(new_client_socket , server_full, strlen(server_full));
            close(new_client_socket);
            continue;
        }

        active_clients[client_id] = new_client_socket;

		// On prend en charge le client dans un nouveau thread.
        pthread_create(&client_threads[client_id], NULL, ( void* (*) (void*) )  handle_client, &client_id);

    }
    close(serv_sock);
    
    return 0;
}
