/**
 * Alexis BEAUJET - M1S2 RXA
 *
 * Client de chat + benchmark réseau
 *
 * Lancement : ./client [echo|ack] <size> SERVER_IP SERVER_PORT
 * 
 * Commandes : voir doc du serveur (en-tête de netbench.c)
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER_PORT 5000

char buffer[256];

typedef enum{
    ECHO_M, ACK_M, UNDEF_M
} bench_mode_t;

typedef struct timing_{
	size_t size;
	unsigned int ps;
	unsigned int time;
	int speed;
	struct timing_* next;
} timing_str;

timing_str* timings = NULL;

timing_str* add_timing(size_t size, unsigned int ps, unsigned int time){
    printf("\n%uB sent in %uB packets in %u seconds\n", size, ps, time);
	timing_str* e = (timing_str*)malloc(sizeof(timing_str));
	e->size = size;
	e->ps = ps;
	e->time = time;
    if(time != 0)
        e->speed = (double)size/(double)time;
    else
        e->speed = size;
	e->next = timings;
	timings = e;
	return e;
}

int del_timings(timing_str* t){
	int c = 0;
	timing_str* e;
	do{
		e = t->next;
		free(t);
		c++;
	}while( (t=e) );
	return c;
}


unsigned int bench(struct sockaddr_in dest, size_t size, bench_mode_t mode){
    int client_sock = socket(AF_INET, SOCK_STREAM, 0);
    time_t begin, end;
    if(client_sock < 0)
        exit(1);

    bool connected = false;
    int conFailed = 0;
    int maxRetries = 2000;
    do{
        if(conFailed > 0 && conFailed%10 == 0)
            putchar('!'), fflush(stdout);
        if(conFailed > maxRetries){
            fprintf(stderr, "Connection failed after %d retries.\n", maxRetries);
            perror("Connect ");
            exit(errno);
        }
        connected = connect(client_sock, (struct sockaddr*)&dest, sizeof(dest)) == 0;
    }while(!connected && ++conFailed);

    sprintf(buffer, "/%s %d", (mode == ACK_M ? "ack" : (mode == ECHO_M ? "echo" : "bye")), size);
    write(client_sock, buffer, strlen(buffer));

    do{
        read(client_sock, buffer, sizeof("begin"));
    }while(strcmp(buffer, "begin") != 0);

    time(&begin);
    if(mode == ECHO_M){
        int c;
        for(int i = 0;i<size;i++){
            write(client_sock, ".", 1);
            read(client_sock, &c, 1);
			if(size%100 == 0){
				putchar('.');
				fflush(stdout);
			}
        }
    }else if(mode == ACK_M){
        char* buff = (char*)malloc(size);
        int written = 0;
        do{
            //printf("WRITE : %d/%d\n", size-written, size);
            written += write(client_sock, buff, size-written);
        }while(written != size);
        free(buff);
    }
    time(&end);

    read(client_sock, buffer, 4); // ack

    write(client_sock, "/bye", 5);
    read(client_sock, buffer, sizeof(buffer)); // bye
    close(client_sock);

    return difftime(end, begin);
}

void output_results_plain(char* filename, timing_str* timings){
	timing_str *pt = timings;
    FILE* f = fopen(filename, "w+");
	while(pt){
        fprintf(f, "%6d %12llu\n", pt->ps, pt->speed);
		pt = pt->next;
	}
    fclose(f);
}

void output_results(timing_str* timings){
	timing_str *pt = timings;
    printf("Packets time\n");
	while(pt){
        printf("%ld octets envoyes en paquets de %6dB : %u B/s.\n", pt->size, pt->ps, pt->speed);
		pt = pt->next;
	}
}

void output_gnuplot(char* filename, char* results_filename, timing_str* timings){
	FILE* gplot = fopen(filename, "w+");
	assert(timings);
	output_results_plain(results_filename, timings);
	fprintf(gplot, "set term png\nset output 'graphique.png'\nset xlabel 'parts sent'\nset ylabel 'transfer speed in Bps'\nshow xlabel\nshow ylabel\nplot '%s' using 1:2 title 'Transfer speeds for %dB.' with lines\n", results_filename, timings->size);
	fclose(gplot);
}

int main(int argc, char* argv[]){

    struct sockaddr_in dest;
    bench_mode_t mode = UNDEF_M;

    if(argc != 7){
        //fprintf(stderr, "Usage : ./client [echo|ack] <size> SERVER_IP SERVER_PORT \n");
        fprintf(stderr, "Usage : ./client [echo|ack] RANGE_BEGIN RANGE_END STEP SERVER_IP SERVER_PORT \n");
        return 1;
    }

    char* mode_str          = argv[1];
    char* range_start_str   = argv[2];
    char* range_end_str     = argv[3];
    char* step_str          = argv[4];
    char* addr_str          = argv[5];
    char* port_str          = argv[6];

    int range_start = atoi(range_start_str);
    int range_end   = atoi(range_end_str);
    int step        = atoi(step_str);

    if(strcmp(mode_str, "echo") == 0)
        mode = ECHO_M;
    else if(strcmp(mode_str, "ack") == 0)
        mode = ACK_M;

    memset(&dest, 0, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(atoi(port_str));

    if( inet_aton(addr_str, &dest.sin_addr.s_addr) == 0 ){
        perror(addr_str);
        exit(errno);
    }

    for(int ps = range_start;ps<=range_end;ps+=step){
        int size = 100000000;
        int count = size/ps;
        printf("bench sz=%d ps=%d count=%d\n", size, ps, count);
        time_t start, end;
        time(&start);
        //int onePercent = count/100;
        //printf("one percent : %d\n", onePercent);
        for(unsigned int j = 1;j<=count;j++){
			//if(j%onePercent == 0){
			if(j%1000 == 0){
				putchar('.');
				fflush(stdout);
			}
            bench(dest, ps, mode);
        }
        time(&end);
        add_timing(size, ps, difftime(end, start));
    }
	
    output_results(timings); // results on screen
    output_gnuplot("results.plot", "results.txt", timings); // plot

    del_timings(timings);

    return 0;
}
